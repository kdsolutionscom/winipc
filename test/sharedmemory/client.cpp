#include <cstdio>
#include <iostream>
#include <string>
#include <cstring>
#include <sstream>

#include <windows.h>

using namespace std;

#include <NamedSharedMemory.h>

using namespace winipc;

int main(int argc, const char** argv)
{
	cout << "open named share memory" << endl;
	std::shared_ptr<NamedSharedMemory> shm = NamedSharedMemory::open("shm1", 100);

	void* buf = shm->getBuffer();

	for (int i = 0; i < 20; i++) {
		char* s = (char*)buf;

		printf("%s\n", s);

		Sleep(100);
	}

	return 0;
}

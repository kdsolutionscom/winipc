#include <cstdio>
#include <iostream>
#include <string>
#include <cstring>
#include <sstream>

#include <windows.h>

using namespace std;

#include <NamedSharedMemory.h>

using namespace winipc;

int main(int argc, const char** argv)
{
	cout << "create named share memory" << endl;
	std::shared_ptr<NamedSharedMemory> shm = NamedSharedMemory::create("shm1", 100);

	void* buf = shm->getBuffer();

	for (int i = 0; i < 10; i++) {
		stringstream ss;
		ss << "test-" << i;
		string s = ss.str();
		memcpy(buf, s.c_str(), s.size() + 1);

		printf("st: %s\n", s.c_str());

		Sleep(1000);
	}
}

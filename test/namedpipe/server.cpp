#include <cstdio>
#include <iostream>
#include <string>

using namespace std;

#include <NamedPipe.h>

using namespace winipc;

int main(int argc, const char** argv)
{
	char d[200];

	cout << "createServer" << endl;
	std::shared_ptr<NamedPipe> np = NamedPipe::createServer("pipename");

	for (;;) {
		cout << "waitForClient" << endl;
		np->waitForClient();

		for (;;) {
			try {
				int r = np->read(d, sizeof(d), 800);
				if (r == 0) {
					cout << "client disconnected" << endl;
					break;
				}

				cout << "read len " << r << "\n";
				cout << "text = " << d << "\n";
			}
			catch (NamedPipeTimeoutException&) {
				cout << "timeout" << endl;
				break;
			}
		}
	}
}

#include <cstdio>
#include <iostream>
#include <string>
#include <windows.h>

using namespace std;

#include <NamedPipe.h>

using namespace winipc;

int main(int argc, const char** argv)
{
	cout << "connectToServer" << endl;
	std::shared_ptr<NamedPipe> np = NamedPipe::connectToServer("pipename");

	const char* text = "test";

	for (int i = 0; i < 5; i++) {
		np->write(text, strlen(text) + 1);
		Sleep(1000);
	}

	return 0;
}

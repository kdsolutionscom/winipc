#include "winipc/utils.h"

#include <string>
#include <sys/time.h>

#include <windows.h>

using namespace std;

namespace winipc
{

string GetLastErrorAsString()
{
	DWORD errorMessageID = ::GetLastError();
	if (errorMessageID == 0)
		return string();

	LPSTR messageBuffer = nullptr;
	size_t size = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
	                             NULL, errorMessageID, MAKELANGID(LANG_SYSTEM_DEFAULT, SUBLANG_DEFAULT), (LPSTR) & messageBuffer, 0, NULL);

	string message(messageBuffer, size);

	LocalFree(messageBuffer);

	string outMessage = "#" + std::to_string(errorMessageID);
	if (!message.empty())
		outMessage += " - " + message;

	return outMessage;
}

uint32_t getTimeMS()
{
	struct timeval tv;
	gettimeofday(&tv, 0);
	uint32_t curTime = tv.tv_sec * 1000 + tv.tv_usec / 1000;
	return curTime;
}

}

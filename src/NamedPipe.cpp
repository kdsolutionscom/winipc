#include <NamedPipe.h>

#include <windows.h>

using namespace std;

namespace winipc
{

class NamedPipe::impl
{
public:
	HANDLE hPipe;
};

NamedPipe::NamedPipe() : pImpl(new NamedPipe::impl()) {}
NamedPipe::~NamedPipe() = default;

NamedPipe::NamedPipe(NamedPipe&& other) noexcept = default;
NamedPipe& NamedPipe::operator=(NamedPipe&&) noexcept = default;

void NamedPipe::waitForClient()
{
	DisconnectNamedPipe(pImpl->hPipe);

	int res = ConnectNamedPipe(pImpl->hPipe, nullptr);

	if (res == 0)
		throw NamedPipeConnectException();
}

int NamedPipe::availableBytes()
{
	DWORD availableBytes = 0;

	if (!PeekNamedPipe(pImpl->hPipe, nullptr, 0, nullptr, &availableBytes, nullptr))
		throw NamedPipeAccessException();

	return availableBytes;
}

int NamedPipe::write(const void* data, int length)
{
	DWORD written;

	if (!WriteFile(pImpl->hPipe, data, length, &written, nullptr)) {
		throw NamedPipeAccessException();
	}

	return written;
}

int NamedPipe::read(void* data, int length, uint32_t timeout)
{
	bool fSuccess;

	if (timeout != 0xffffffff) {
		uint32_t curTime = getTimeMS();
		uint32_t endTime = curTime + timeout;

		if (availableBytes() > 0)
			goto do_read;

		while (curTime < endTime) {
			if (availableBytes() > 0)
				goto do_read;

			Sleep(10);
			curTime = getTimeMS();
		}

		if (availableBytes() > 0)
			goto do_read;
		else
			throw NamedPipeTimeoutException();
	}
	else {
		goto do_read;
	}

do_read:
	DWORD read;

	fSuccess = ReadFile(
					pImpl->hPipe, // pipe handle
					data,         // buffer to receive reply
					length,       // size of buffer
					&read,        // number of bytes read
					nullptr);     // not overlapped

	if (!fSuccess) {
		if (GetLastError() == ERROR_BROKEN_PIPE)
			return 0;
		else
			throw NamedPipeAccessException();
	}

	return read;
}

std::shared_ptr<NamedPipe> NamedPipe::createServer(const string& name, int sendBufferSize, int recvBufferSize)
{
	std::shared_ptr<NamedPipe> np = std::shared_ptr<NamedPipe>(new NamedPipe());

	string fullName = R"(\\.\pipe\)" + name;

	HANDLE hPipe = CreateNamedPipeA(fullName.c_str(),
	                                PIPE_ACCESS_DUPLEX,
	                                PIPE_TYPE_BYTE | PIPE_READMODE_BYTE |
	                                PIPE_WAIT,
	                                1,
	                                sendBufferSize,
	                                recvBufferSize,
	                                NMPWAIT_USE_DEFAULT_WAIT,
	                                nullptr);

	if (hPipe == INVALID_HANDLE_VALUE)
		throw NamedPipeCreationFailedException();

	np->pImpl->hPipe = hPipe;

	return np;
}

std::shared_ptr<NamedPipe> NamedPipe::connectToServer(const string& name)
{
	std::shared_ptr<NamedPipe> np = std::shared_ptr<NamedPipe>(new NamedPipe());

	string fullName = R"(\\.\pipe\)" + name;

	if (!WaitNamedPipe(fullName.c_str(), 0))
		throw NamedPipeConnectException();

	HANDLE hPipe = CreateFile(
					fullName.c_str(), // pipe name
					GENERIC_READ |    // read and write access
					GENERIC_WRITE,
					0,                // no sharing
					nullptr,          // default security attributes
					OPEN_EXISTING,    // opens existing pipe
					0,                // default attributes
					nullptr);         // no template file

	if (hPipe == INVALID_HANDLE_VALUE)
		throw NamedPipeConnectException();

	DWORD dwMode = PIPE_READMODE_BYTE | PIPE_WAIT;
	bool fSuccess = SetNamedPipeHandleState(
					hPipe,    // pipe handle
					&dwMode,  // new pipe mode
					nullptr,  // don't set maximum bytes
					nullptr); // don't set maximum time
	if (!fSuccess) {
		CloseHandle(hPipe);
		throw NamedPipeConnectException();
	}

	np->pImpl->hPipe = hPipe;

	return np;
}

}

#include <NamedSharedMemory.h>

#include <windows.h>

using namespace std;

namespace winipc
{

class NamedSharedMemory::impl
{
public:
	HANDLE hMapFile;
	void* buffer;
};

NamedSharedMemory::NamedSharedMemory() : pImpl(new NamedSharedMemory::impl()) {}
NamedSharedMemory::~NamedSharedMemory()
{
	UnmapViewOfFile(pImpl->buffer);
	CloseHandle(pImpl->hMapFile);
}

NamedSharedMemory::NamedSharedMemory(NamedSharedMemory&& other) noexcept = default;
NamedSharedMemory& NamedSharedMemory::operator=(NamedSharedMemory&&) noexcept = default;

void* NamedSharedMemory::getBuffer() { return pImpl->buffer; }

std::shared_ptr<NamedSharedMemory> NamedSharedMemory::create(const std::string& name, uint64_t maxSize)
{
	std::shared_ptr<NamedSharedMemory> np = std::shared_ptr<NamedSharedMemory>(new NamedSharedMemory());

	np->pImpl->hMapFile = CreateFileMappingA(
					INVALID_HANDLE_VALUE,         // use paging file
					nullptr,                      // default security
					PAGE_READWRITE,               // read/write access
					(maxSize >> 32u) & 0xffffffu, // maximum object size (high-order DWORD)
					(maxSize >> 0u) & 0xffffffu,  // maximum object size (low-order DWORD)
					name.c_str());                // name of mapping object

	if (np->pImpl->hMapFile == nullptr)
		throw NamedSharedMemoryCreationException();

	np->pImpl->buffer = MapViewOfFile(
					np->pImpl->hMapFile,  // handle to map object
					FILE_MAP_ALL_ACCESS,  // read/write permission
					0,
					0,
					maxSize);

	if (np->pImpl->buffer == nullptr) {
		CloseHandle(np->pImpl->hMapFile);
		throw NamedSharedMemoryCreationException();
	}

	return np;
}

std::shared_ptr<NamedSharedMemory> NamedSharedMemory::open(const std::string& name, uint64_t size)
{
	std::shared_ptr<NamedSharedMemory> np = std::shared_ptr<NamedSharedMemory>(new NamedSharedMemory());

	np->pImpl->hMapFile = OpenFileMapping(
					FILE_MAP_ALL_ACCESS,   // read/write access
					FALSE,                 // do not inherit the name
					name.c_str());         // name of mapping object

	if (np->pImpl->hMapFile == nullptr) {
		if (GetLastError() == ERROR_FILE_NOT_FOUND)
			throw NamedSharedMemoryNotFoundException();
		else
			throw NamedSharedMemoryConnectException();
	}

	np->pImpl->buffer = MapViewOfFile(
					np->pImpl->hMapFile,  // handle to map object
					FILE_MAP_ALL_ACCESS,  // read/write permission
					0,
					0,
					size);

	if (np->pImpl->buffer == nullptr) {
		CloseHandle(np->pImpl->hMapFile);
		throw NamedSharedMemoryCreationException();
	}

	return np;
}

}

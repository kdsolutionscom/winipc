cmake_minimum_required(VERSION 3.10)

add_library(winipc
		${CMAKE_CURRENT_LIST_DIR}/src/utils.cpp
		${CMAKE_CURRENT_LIST_DIR}/src/NamedSharedMemory.cpp
		${CMAKE_CURRENT_LIST_DIR}/src/NamedPipe.cpp
		)
target_include_directories(winipc PUBLIC ${CMAKE_CURRENT_LIST_DIR}/include/)

function(add_winipc_to_target target)
	target_link_libraries(${target} PRIVATE winipc)
	target_link_libraries(${target} PUBLIC ws2_32)
endfunction()

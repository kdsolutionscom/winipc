#pragma once

#include <string>
#include <memory>

#include "winipc/common.h"
#include "winipc/utils.h"

namespace winipc
{
struct NamedPipeCreationFailedException : public WindowsException
{
public:
	NamedPipeCreationFailedException() : WindowsException("named pipe creation failed") {}
};

struct NamedPipeConnectException : public WindowsException
{
public:
	NamedPipeConnectException() : WindowsException("named pipe connect failed") {}
};

struct NamedPipeAccessException : public WindowsException
{
public:
	NamedPipeAccessException() : WindowsException("named pipe failed") {}
};

struct NamedPipeClosedException : public WindowsException
{
public:
	NamedPipeClosedException() : WindowsException("named pipe failed") {}
};

struct NamedPipeTimeoutException : public WindowsException
{
public:
	NamedPipeTimeoutException() : WindowsException("named pipe timeout") {}
};

class NamedPipe
{
	class impl;

	std::unique_ptr<impl> pImpl;

	NamedPipe();

public:
	~NamedPipe();

	NamedPipe(NamedPipe&& other) noexcept;
	NamedPipe& operator=(NamedPipe&&) noexcept;

	void waitForClient();

	int availableBytes();

	int write(const void* data, int length);
	int read(void* data, int length, uint32_t timeout);

	static std::shared_ptr<NamedPipe> createServer(const std::string& name, int sendBufferSize = 16 * 1024, int recvBufferSize = 16 * 1024);
	static std::shared_ptr<NamedPipe> connectToServer(const std::string& name);
};

}

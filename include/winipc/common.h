#pragma once

#include <string>

#include "utils.h"

namespace winipc
{
struct WindowsException : public std::exception
{
private:
	std::string msg;

public:
	explicit WindowsException(const std::string& msg) : msg(msg + " (" + GetLastErrorAsString() + ")") {}

	const char* what() const noexcept override { return msg.c_str(); }
};
}

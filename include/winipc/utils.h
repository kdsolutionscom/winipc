#pragma once

#include <string>

namespace winipc
{
std::string GetLastErrorAsString();
uint32_t getTimeMS();
}
#pragma once

#include <string>
#include <memory>

#include "winipc/common.h"
#include "winipc/utils.h"

namespace winipc
{
struct NamedSharedMemoryCreationException : public WindowsException
{
public:
	NamedSharedMemoryCreationException() : WindowsException("named pipe creation failed") {}
};

struct NamedSharedMemoryConnectException : public WindowsException
{
public:
	NamedSharedMemoryConnectException() : WindowsException("named pipe connect failed") {}
};

struct NamedSharedMemoryNotFoundException : public WindowsException
{
public:
	NamedSharedMemoryNotFoundException() : WindowsException("named pipe not found") {}
};

class NamedSharedMemory
{
	class impl;

	std::unique_ptr<impl> pImpl;

	NamedSharedMemory();

public:
	~NamedSharedMemory();

	NamedSharedMemory(NamedSharedMemory&& other) noexcept;
	NamedSharedMemory& operator=(NamedSharedMemory&&) noexcept;

	void* getBuffer();

	static std::shared_ptr<NamedSharedMemory> create(const std::string& name, uint64_t maxSize);
	static std::shared_ptr<NamedSharedMemory> open(const std::string& name, uint64_t size);
};

}
